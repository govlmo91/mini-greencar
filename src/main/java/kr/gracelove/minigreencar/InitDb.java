package kr.gracelove.minigreencar;

import kr.gracelove.minigreencar.domain.car.CarEntity;
import kr.gracelove.minigreencar.domain.car.CarType;
import kr.gracelove.minigreencar.domain.member.MemberEntity;
import kr.gracelove.minigreencar.repository.car.CarRepository;
import kr.gracelove.minigreencar.repository.member.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class InitDb {

    private final MemberRepository memberRepository;
    private final CarRepository carRepository;

    @PostConstruct
    public void init() {
        IntStream.range(1, 10).forEach(index -> {
            MemberEntity mem = MemberEntity.builder().username("test" + index).email("govlmo" + index + "@gmail.com").build();
            memberRepository.save(mem);

            CarEntity car = CarEntity.builder().name("붕붕이"+index).model(CarType.K5).build();
            carRepository.save(car);
        });
    }
}
