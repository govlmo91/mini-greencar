package kr.gracelove.minigreencar.domain.car.dto;

import kr.gracelove.minigreencar.domain.car.CarEntity;
import kr.gracelove.minigreencar.domain.car.CarType;
import lombok.Getter;

@Getter
public class CarSearchResponseDto {
    private Long id;
    private String name;
    private CarType model;

    public CarSearchResponseDto(CarEntity carEntity) {
        this.id = carEntity.getId();
        this.name = carEntity.getName();
        this.model = carEntity.getModel();
    }
}
