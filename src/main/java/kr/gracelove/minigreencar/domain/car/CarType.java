package kr.gracelove.minigreencar.domain.car;

public enum CarType {
    SONATA, K5, SPARK;
}
