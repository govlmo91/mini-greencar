package kr.gracelove.minigreencar.domain.car;

import kr.gracelove.minigreencar.domain.car.dto.CarSearchResponseDto;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor
public class CarEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Enumerated(EnumType.STRING)
    private CarType model;

    @Builder
    public CarEntity(String name, CarType model) {
        this.name = name;
        this.model = model;
    }
}
