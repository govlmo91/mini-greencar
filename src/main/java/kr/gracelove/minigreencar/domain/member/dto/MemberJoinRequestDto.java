package kr.gracelove.minigreencar.domain.member.dto;

import kr.gracelove.minigreencar.domain.member.MemberEntity;
import lombok.Builder;
import lombok.Getter;

@Getter
public class MemberJoinRequestDto {
    private String username;
    private String email;

    @Builder
    public MemberJoinRequestDto(MemberEntity member) {
        this.username = member.getUsername();
        this.email = member.getEmail();
    }
}
