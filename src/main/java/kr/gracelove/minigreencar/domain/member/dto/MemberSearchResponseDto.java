package kr.gracelove.minigreencar.domain.member.dto;

import kr.gracelove.minigreencar.domain.member.MemberEntity;
import lombok.Getter;

@Getter
public class MemberSearchResponseDto {
    private Long id;
    private String username;
    private String email;

    public MemberSearchResponseDto(MemberEntity memberEntity) {
        this.id = memberEntity.getId();
        this.username = memberEntity.getUsername();
        this.email = memberEntity.getEmail();
    }
}
