package kr.gracelove.minigreencar.domain.member;


import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Entity
@Getter
@NoArgsConstructor
public class MemberEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    @Email
    private String email;

    @Transient
    private String password;

    @Transient
    private String password2;

    @Builder
    public MemberEntity(String username, String email) {
        this.username = username;
        this.email = email;
    }

}
