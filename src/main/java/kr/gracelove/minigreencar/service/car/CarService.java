package kr.gracelove.minigreencar.service.car;

import kr.gracelove.minigreencar.domain.car.CarEntity;
import kr.gracelove.minigreencar.domain.car.dto.CarSearchResponseDto;
import kr.gracelove.minigreencar.repository.car.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository carRepository;


    public CarEntity getCar(Long id) {

        return carRepository.findById(id).orElseThrow();

    }

    public List<CarEntity> getCars() {
        return carRepository.findAll();
    }

    public CarEntity registerCar(CarEntity car) {
        return carRepository.save(car);
    }

    public boolean deleteCar(Long id) {
        try {
            carRepository.deleteById(id);
        }catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
