package kr.gracelove.minigreencar.service.member;

import kr.gracelove.minigreencar.domain.member.MemberEntity;
import kr.gracelove.minigreencar.repository.member.MemberRepository;
import kr.gracelove.minigreencar.domain.member.dto.MemberJoinRequestDto;
import kr.gracelove.minigreencar.domain.member.dto.MemberSearchResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository memberRepository;

    public Long joinMember(MemberJoinRequestDto requestDto) {
        MemberEntity memberEntity = MemberEntity.builder()
                .username(requestDto.getUsername())
                .email(requestDto.getEmail()).build();

        return memberRepository.save(memberEntity).getId();
    }

    public List<MemberSearchResponseDto> getMembers() {
        return memberRepository.findAll().stream().map(MemberSearchResponseDto::new).collect(Collectors.toList());
    }

    public MemberSearchResponseDto getMember(Long id) {
        return memberRepository.findById(id).map(MemberSearchResponseDto::new).orElseThrow();
    }
}
