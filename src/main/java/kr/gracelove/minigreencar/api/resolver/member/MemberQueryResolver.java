package kr.gracelove.minigreencar.api.resolver.member;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import kr.gracelove.minigreencar.service.member.MemberService;
import kr.gracelove.minigreencar.domain.member.dto.MemberSearchResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MemberQueryResolver implements GraphQLQueryResolver {

    private final MemberService memberService;

    public MemberSearchResponseDto member(Long id) {
        return memberService.getMember(id);
    }

    public List<MemberSearchResponseDto> allMembers() {
        return memberService.getMembers();
    }

}
