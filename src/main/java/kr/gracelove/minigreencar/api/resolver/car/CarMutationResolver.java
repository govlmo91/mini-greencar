package kr.gracelove.minigreencar.api.resolver.car;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import kr.gracelove.minigreencar.domain.car.CarEntity;
import kr.gracelove.minigreencar.domain.car.CarType;
import kr.gracelove.minigreencar.domain.car.dto.CarSearchResponseDto;
import kr.gracelove.minigreencar.service.car.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CarMutationResolver implements GraphQLMutationResolver {

    private final CarService carService;

    public CarEntity newCar(String name, CarType model) {
        CarEntity car = CarEntity.builder().name(name).model(model).build();
        return carService.registerCar(car);
    }

    public boolean deleteCarById(Long id) {
        return carService.deleteCar(id);
    }


}
