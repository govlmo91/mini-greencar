package kr.gracelove.minigreencar.api.resolver.car;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import kr.gracelove.minigreencar.domain.car.CarEntity;
import kr.gracelove.minigreencar.domain.car.dto.CarSearchResponseDto;
import kr.gracelove.minigreencar.service.car.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class CarQueryResolver implements GraphQLQueryResolver {

    private final CarService carService;

    public CarEntity car(Long id) {
        return carService.getCar(id);
    }

    public List<CarEntity> allCars() {
        return carService.getCars();
    }

}
