package kr.gracelove.minigreencar.repository.member;

import kr.gracelove.minigreencar.domain.member.MemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<MemberEntity, Long> {
}
