package kr.gracelove.minigreencar.repository.car;

import kr.gracelove.minigreencar.domain.car.CarEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<CarEntity, Long> {
}
