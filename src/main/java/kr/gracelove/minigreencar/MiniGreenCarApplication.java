package kr.gracelove.minigreencar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniGreenCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiniGreenCarApplication.class, args);
	}

}
